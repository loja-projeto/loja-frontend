import { createRouter, createWebHistory } from 'vue-router'
import Home from '../components/home/Index.vue'
import Produtos from '../components/produtos/Index.vue'
import Cadastrar from '../components/usuario/cadastrar/Index.vue'
import Login from '../components/usuario/login/Index.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/produtos',
    name: 'Produtos',
    component: Produtos
  },
  {
    path: '/usuario/cadastrar',
    name: 'Cadastrar',
    component: Cadastrar
  },
  {
    path: '/usuario/login',
    name: 'Login',
    component: Login
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
