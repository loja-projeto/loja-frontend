import axios from 'axios'
import sessao from '../sessao/Index'
import router from '../../router'

const instancia = axios.create({
    timeout:1000,
    headers: {'Authorization': `Bearer ${sessao.recuperaSessaoToken()}`}
})

const requisicaoPOST= (url, payload = null) => {
    return instancia.post(url,payload)
        .then(response => response)
        .catch(e => {
            if (e.response.status === 401) {
              router.push({name: 'login'})
            }
        })
}

const requisicaoGET=(url, params = null) => {
    return instancia.get(url, {params})
        .then(response => response)
}

export {
    requisicaoPOST,
    requisicaoGET
}