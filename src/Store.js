import { reactive } from "@vue/reactivity"

const getState = reactive ({
    produtos: [
        {
            nome: "Mouse",
            preco: "150.00",
            descricao: "Mouse Gamer "
        },
        {
            nome: "Tv",
            preco: "1250.00",
            descricao: "Tv bala"
        },
        {
            nome: "Cadeira",
            preco: "250.00",
            descricao: "Cadeira Gamer"
        }
    ],
    resultadoPesquisa: [],
    itemsCarrinho: []
})


const setState = (propriedade, valor)  => {
    getState[propriedade] = valor
}

const setNotificacao = (obj) => {
    getState.notificacao = obj

    setTimeout(() => {
        getState.notificacao.show = false
        getState.notificacao.mensagem = null
    }, 3000)
}

const adicionarItemsCarrinho = (produto) => {
    getState.itemsCarrinho.push(produto)
    console.log(produto)
}

const removeItensCarrinho = (indice) => {
    getState.itemsCarrinho.splice(indice, 1)
}

export {
    getState,
    removeItensCarrinho,
    setNotificacao,
    adicionarItemsCarrinho,
    setState
}
